// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.7;

import "@openzeppelin/contracts/access/Ownable.sol";

import {FxBaseChildTunnel} from "../tunnel/FxBaseChildTunnel.sol";
import "../pools/IChildPool.sol";

/**
 * @title FxStateChildTunnel
 */
contract FxStateChildTunnel is FxBaseChildTunnel, Ownable {
    uint256 public latestStateId;
    address public latestRootMessageSender;
    bytes public latestData;
    address public pool;

    constructor(address _fxChild) FxBaseChildTunnel(_fxChild) {}

    /**
     * @notice Process message received from Root Tunnel
     * @dev function needs to be implemented to handle message as per requirement
     * This is called by onStateReceive function.
     * Since it is called via a system call, any event will not be emitted during its execution.
     * @param data bytes message that was sent from Root Tunnel
     */
    function _processMessageFromRoot(
        uint256 stateId,
        address sender,
        bytes memory data
    ) internal override validateSender(sender) {
        latestStateId = stateId;
        latestRootMessageSender = sender;
        latestData = data;
    }

    /**
     * @notice Emit message that can be received on Root Tunnel
     * @dev Call the internal function when need to emit message
     * @param message bytes message that will be sent to Root Tunnel
     * some message examples -
     *   abi.encode(tokenId);
     *   abi.encode(tokenId, tokenMetadata);
     *   abi.encode(messageType, messageData);
     */
    function sendMessageToRoot(bytes memory message) public {
        require(msg.sender == pool, "!pool");

        _sendMessageToRoot(message);
    }

    /**
     * @notice Reads the latest data from child
     * @return shuttleNumber shuttle number
     * @return amount amount of wstETH in wei
     * @return shuttleProcessingStatus shuttle status
    */
    function readData() external view returns (uint256, uint256, ShuttleProcessingStatus) {
      (uint256 shuttleNumber, uint256 amount, ShuttleProcessingStatus shuttleProcessingStatus) = abi.decode(
            latestData,
            (uint256, uint256, ShuttleProcessingStatus)
        );

        return (shuttleNumber, amount, shuttleProcessingStatus);
    }

    /** Setter */


    /**
     * @notice This will set address of root pool contract 
     * @param _pool Address of root pool contract. 
     */
    function setPool(address _pool) external onlyOwner {
        pool = _pool;
    }
}
