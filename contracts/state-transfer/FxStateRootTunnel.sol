// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.7;

import "@openzeppelin/contracts/access/Ownable.sol";

import {FxBaseRootTunnel} from "../tunnel/FxBaseRootTunnel.sol";

/**
 * @title FxStateRootTunnel
*/
contract FxStateRootTunnel is FxBaseRootTunnel, Ownable {
    bytes public latestData;
    address public pool;

    constructor(address _checkpointManager, address _fxRoot) FxBaseRootTunnel(_checkpointManager, _fxRoot) {}

    /**
     * @notice Process message received from Child Tunnel
     * @dev function needs to be implemented to handle message as per requirement
     * This is called by the receiveMessage() function.
     * Since it is called via a system call, any event will not be emitted during its execution.
     * @param data bytes message that was sent from child tunnel
    */
    function _processMessageFromChild(bytes memory data) internal override {
        latestData = data;
    }
    /**
     * @notice Send bytes message to Child Tunnel
     * @param message bytes message that will be sent to Child Tunnel
     * some message examples -
     *   abi.encode(tokenId);
     *   abi.encode(tokenId, tokenMetadata);
     *   abi.encode(messageType, messageData);
     */
    function sendMessageToChild(bytes memory message) public {
        require(msg.sender == pool, "!pool");

        _sendMessageToChild(message);
    }

    /**
     * @notice Reads the latest data from child
     * @return shuttleNumber shuttle number
     * @return amount amount if wETH in wei
     */
    function readData() public view returns (uint256, uint256) {
      (uint256 batchNumber, uint256 amount) = abi.decode(
            latestData,
            (uint256, uint256)
        );

        return (batchNumber,amount);
    }
    /** Setter */


    /**
     * @notice This will set address of root pool contract 
     * @param _pool Address of root pool contract. 
     */
    function setPool(address _pool) external onlyOwner {
        pool = _pool;
    }
}
