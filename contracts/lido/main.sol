// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.7;

import {SafeMath} from "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts-upgradeable/interfaces/IERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";

import {Helpers} from "./helpers.sol";
import "./interface.sol";
import "../lib/TokenInterface.sol";

/**
 * @title Lido staking and Polygon bridging wrapper contract
 */
contract LidoAdapter is Helpers {
    using SafeMath for uint256;
    using SafeERC20Upgradeable for IERC20Upgradeable;

    event Deposited(
        address _beneficiary,
        uint256 _eth,
        uint256 _wstEth,
        bool _isBridged
    );


    /**
     * @notice It performs staking to lido and bridges wstETH to
     *         Polygon PoS network.
     * @param _beneficiary Beneficiary address which will receive wstETH token.
     * @return amount of wstETH that was sent to Polygon PoS
     */
    function depositForAndBridge(address _beneficiary)
        external
        payable
        returns (uint256)
    {
        uint256 wstTokenAmount = _stake();
        _bridgeToMatic(wstTokenAmount, _beneficiary);

        return wstTokenAmount;
    }

    /**
     * @notice Send wstETH tokens to _beneficiary on Polygon PoS network.
     * @param _wstTokenAmount Amount of wstETH to be bridged
     * @param _beneficiary Beneficiary address which will receive wstETH token.
     */
    function _bridgeToMatic(uint256 _wstTokenAmount, address _beneficiary)
        private
    {
        IERC20Upgradeable(address(wstEthProxy)).safeApprove(
            ERC20PredicateProxy,
            0
        );
        IERC20Upgradeable(address(wstEthProxy)).safeApprove(
            ERC20PredicateProxy,
            _wstTokenAmount
        );

        bytes memory depositData = abi.encode(_wstTokenAmount);

        rootChainManagerProxy.depositFor(
            _beneficiary,
            address(wstEthProxy),
            depositData
        );
    }


    /**
     * @notice Perform staking to lido and returns amount of wstETH tokens.
     * @return amount of wstETH acquired
     */
    function _stake() private returns (uint256) {
        require(msg.value > 0, "Invalid amount");
        uint256 stTokenAmount = stEthProxy.submit{value: msg.value}(address(0));
        stEthProxy.approve(address(wstEthProxy), stTokenAmount);
        wstEthProxy.wrap(stTokenAmount);

        return wstEthProxy.balanceOf(address(this));
    }
}
