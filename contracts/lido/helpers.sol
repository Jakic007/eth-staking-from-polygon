// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.7;

import "@openzeppelin/contracts-upgradeable/interfaces/IERC20Upgradeable.sol";

import {DSMath} from "./math.sol";
import {Basic} from "./basic.sol";

import "./interface.sol";

/**
 * @title Helper contract for locating onchain contracts
 * @dev Replace the addresses for mainnet and testnet before deployment
*/
abstract contract Helpers is DSMath, Basic {

    // Mainnet: 0xae7ab96520DE3A18E5e111B5EaAb095312D7fE84
    // Testnet: 0x2DD6530F136D2B56330792D46aF959D9EA62E276
    StEthProxy public constant stEthProxy =
        StEthProxy(0x2DD6530F136D2B56330792D46aF959D9EA62E276);

    // Mainnet: 0x7f39C581F595B53c5cb19bD0b3f8dA6c935E2Ca0
    // Testnet: 0x4942BBAf745f235e525BAff49D31450810EDed5b - mock
    WstEthProxy public constant wstEthProxy =
        WstEthProxy(0x4942BBAf745f235e525BAff49D31450810EDed5b);


    // Mainnet: 0xA0c68C638235ee32657e8f720a23ceC1bFc77C77
    // Testnet: 0xBbD7cBFA79faee899Eaf900F13C9065bF03B1A74
    RootchainManagerProxy public constant rootChainManagerProxy =
        RootchainManagerProxy(0xBbD7cBFA79faee899Eaf900F13C9065bF03B1A74);

    // Mainnet: 
    // Testnet: 0xdD6596F2029e6233DEFfaCa316e6A95217d4Dc34
    address public constant ERC20PredicateProxy =
        address(0xdD6596F2029e6233DEFfaCa316e6A95217d4Dc34);

}
