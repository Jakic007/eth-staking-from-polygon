// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.7;

import "@openzeppelin/contracts-upgradeable/interfaces/IERC20Upgradeable.sol";

/**
 * @title Interfaces for interacting with stETH,wstETH token contract functions and Polygon Root Chain Manager
*/
interface StEthProxy {
    /**
     * @dev Stakes ETH to lido, needs to have attached ETH to the call
     * @param _referral Optional referal address for tracking deposits
     * @return amount in stETH that was sent to the depositor
    */
    function submit(address _referral) external payable returns (uint256);
    function approve(address spender, uint256 amount) external returns (bool);
}

interface WstEthProxy {
    /**
     * @dev Receives stETH deposit, wraps it into non-rebasable wstETH
     * @param _stETHAmount amount of stETH to be wrapped in wei
     * @return amount of wstETH user receives after wrap
    */
    function wrap(uint256 _stETHAmount) external returns (uint256);
    function balanceOf(address account) external view returns (uint256);
}

interface RootchainManagerProxy {
    /**
     * @notice Move tokens from root to child chain
     * @dev This mechanism supports arbitrary tokens as long as its predicate has been registered and the token is mapped
     * @param user address of account that should receive this deposit on child chain
     * @param rootToken address of token that is being deposited
     * @param depositData bytes data that is sent to predicate and child token contracts to handle deposit
    */
    function depositFor(
        address user,
        address rootToken,
        bytes calldata depositData
    ) external;

}

