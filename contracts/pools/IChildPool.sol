// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.7;

contract IChildPool {
    enum ShuttleStatus {
        UNAVAILABLE,
        AVAILABLE,
        ENROUTE,
        ARRIVED,
        EXPIRED,
        CANCELLED
    }

    struct Shuttle {
        uint256 totalAmount;
        ShuttleStatus status;
        uint256 recievedToken;
        uint256 expiry;
    }

    event ShuttleCreated(uint256 _shuttleNumber);
    event Deposit(uint256 _shuttlesNumber, address _sender, uint256 _amount);
    event ShuttleEnrouted(uint256 _shuttleNumber, uint256 _amount);
    event ShuttleArrived(
        uint256 _shuttleNumber,
        uint256 _amount,
        ShuttleStatus _status
    );
    event TokenClaimed(
        uint256 _shuttleNumber,
        address _token,
        address _beneficiary,
        uint256 _claimedAmount
    );

    event ShuttleExpired(uint256 _shuttleNumber);
    event ShuttleCancelled(uint256 _shuttleNumber);
    event ShuttleExpiryChanged(uint256 _shuttleExpiry);

    error ZeroAddress();
}

enum ShuttleProcessingStatus {
    PROCESSED,
    CANCELLED
}

/**
 * @dev Interface of a Child Tunnel contract that is used to send and receive messages through the bridge
*/
interface IFxStateChildTunnel {
    /**
     * @dev Send the message to the root chain
     * @param message is a byte encoded message
    */
    function sendMessageToRoot(bytes memory message) external;

    /**
     * @dev Reads the data about the shuttle received from the root chain
     * @return shuttleNumber shuttle number
     * @return amount amount of wstETH in wei
     * @return status shuttle status
    */
    function readData()
        external
        returns (
            uint256,
            uint256,
            ShuttleProcessingStatus
        );
}

/**
 * @dev Interface of a child wETH token
*/
interface IWEthToken {
    /**
     * @dev withdraws the token to the same address on Ethereum
    */
    function withdraw(uint256) external payable;
    function transferFrom(
        address from,
        address to,
        uint256 amount
    ) external returns (bool);
    
    function transfer(
        address to,
        uint256 amount
    ) external returns (bool);

    function balanceOf(address account) external view returns (uint256);

}


