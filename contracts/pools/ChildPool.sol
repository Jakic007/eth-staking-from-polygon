// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.7;

import {SafeMath} from "@openzeppelin/contracts/utils/math/SafeMath.sol";
import {IERC20} from "@openzeppelin/contracts/token/ERC20/IERC20.sol";

import "./IChildPool.sol";
import "./PoolSecurityModule.sol";

/**
 * @title Child Pool contract
 * @dev Manages user deposits and claims on child chain.
 */
contract ChildPool is IChildPool, PoolSecurityModule {
    using SafeMath for uint256;

    IFxStateChildTunnel public childTunnel;
    IWEthToken public wEthToken;
    IERC20 public wstEthToken;
    uint256 public shuttleExpiry;
    uint256 public currentShuttle;
    uint256 public enroutedShuttle;
    uint256 public availableWEthBalance;
    uint256 public availableWstEthBalance;

    mapping(uint256 => Shuttle) public shuttles;
    mapping(uint256 => mapping(address => uint256)) public balances;
    
    /**
     * @dev Initialize the contract, setup roles and create first shuttle
     *
     * @param _childTunnel - Address of the child tunnel.
     * @param _wEthToken - Address of wETH token on Polygon PoS
     * @param _wstEthToken - Address of wstEth on Polygon PoS
     * @param _shuttleExpiry - Expiry time of shuttle in blocks
     * @param _owner - Address of the owner
     */
    function initialize(
        IFxStateChildTunnel _childTunnel,
        IWEthToken _wEthToken,
        IERC20 _wstEthToken,
        uint256 _shuttleExpiry,
        address _owner
    ) public initializer {
        __AccessControl_init();
        __Pausable_init();
        __ReentrancyGuard_init();

        childTunnel = _childTunnel;
        wEthToken = _wEthToken;
        wstEthToken = _wstEthToken;
        shuttleExpiry = _shuttleExpiry;

        _setupRole(DEFAULT_ADMIN_ROLE, _owner);
        _setupRole(OPERATOR_ROLE, _owner);
        _setupRole(PAUSE_ROLE, _owner);
        _setupRole(GOVERNANCE_ROLE, _owner);

        _createNewShuttle();
    }

    /**
     * @dev Increments a shuttle index
     */
    function _createNewShuttle() internal {
        currentShuttle = currentShuttle.add(1);
        shuttles[currentShuttle] = Shuttle({
            totalAmount: 0,
            status: ShuttleStatus.AVAILABLE,
            recievedToken: 0,
            expiry: block.number.add(shuttleExpiry)
        });
        emit ShuttleCreated(currentShuttle);
    }

    /**
     * @notice Deposit wETH tokens to the current shuttle
     * @dev Deposited tokens get increase the balance of the current shuttle
     */
    function deposit(uint256 amount) external whenNotPaused {
        require(amount > 0, "!amount");
        wEthToken.transferFrom(msg.sender, address(this), amount);

        balances[currentShuttle][msg.sender] = balances[currentShuttle][
            msg.sender
        ].add(amount);

        shuttles[currentShuttle].totalAmount = shuttles[currentShuttle]
            .totalAmount
            .add(amount);

        availableWEthBalance = availableWEthBalance.add(amount);

        emit Deposit(currentShuttle, msg.sender, amount);
    }

    /**
     * @notice Sends the shuttle to Ethereum, only callable by the operator
     * @dev Enroute Shuttle: Trigger crosschain transfer of funds
     *   - Withdraw funds to rootPool
     *   - Send a message to rootPool
     *
     * @param _shuttleNumber Shuttle Number that should be enrouted.
     *
     */
    function enrouteShuttle(uint256 _shuttleNumber)
        external
        whenNotPaused
        onlyRole(OPERATOR_ROLE)
    {
        require(enroutedShuttle == 0, "!already enrouted shuttle");
        require(
            shuttles[_shuttleNumber].status == ShuttleStatus.AVAILABLE,
            "!status"
        );
        uint256 amount = shuttles[_shuttleNumber].totalAmount;
        require(amount > 0, "!amount");

        enroutedShuttle = _shuttleNumber;
        shuttles[_shuttleNumber].status = ShuttleStatus.ENROUTE;

        availableWEthBalance = availableWEthBalance.sub(amount);
        // only works if the RootPool is on the same address
        wEthToken.withdraw(amount);
        childTunnel.sendMessageToRoot(abi.encode(enroutedShuttle, amount));

        _createNewShuttle();

        emit ShuttleEnrouted(enroutedShuttle, amount);
    }

    /**
     * @notice Finalizes the shuttle, callable by the operator. Users can claim the funds after.
     * @dev This function will be called by operator once funds and message is recieved from root chain. There are two different kind of messages that can be recieved.
     * 1. PROCESSED: If the shuttle on the root chain is processed, then this function will change shuttle status to ARRIVED and users can claim wstEth
     * 2. CANCELLED: If the shuttle on the root chain is cancelled, then this function will change shuttle status to CANCELLED and users can claim wETH token
     * 3. EXPIRED: If the shuttle on the child chain was not sent before the expiration time and someone called the expireShuttle function
     * @param _shuttleNumber Shuttle number that should be marked as arrived.
     *
     */
    function arriveShuttle(uint256 _shuttleNumber)
        external
        whenNotPaused
        onlyRole(OPERATOR_ROLE)
    {
        require(
            enroutedShuttle == _shuttleNumber,
            "!Shuttle should be enrouted"
        );
        require(
            shuttles[_shuttleNumber].status == ShuttleStatus.ENROUTE,
            "!status"
        );

        (
            uint256 shuttleNumber,
            uint256 amount,
            ShuttleProcessingStatus shuttleProcessingStatus
        ) = childTunnel.readData();

        require(
            shuttleNumber == _shuttleNumber,
            "!shuttle message not recieved"
        );

        // reset it so that next shuttle can be enrouted
        enroutedShuttle = 0;

        if (shuttleProcessingStatus == ShuttleProcessingStatus.PROCESSED) {
            // make sure wstEth is arrived from bridge
            require(
                wstEthToken.balanceOf(address(this)) >=
                    availableWstEthBalance.add(amount),
                "!insufficient wstEth balance"
            );

            uint256 recievedToken = amount;
            availableWstEthBalance = availableWstEthBalance.add(
                recievedToken
            );

            shuttles[_shuttleNumber].recievedToken = recievedToken;
            shuttles[_shuttleNumber].status = ShuttleStatus.ARRIVED;

        } 

        emit ShuttleArrived(
            shuttleNumber,
            amount,
            shuttles[_shuttleNumber].status
        );
    }

    /**
     * @notice Returns the estimated amount of wstEth token for claiming
     * @param _balance User's balance in the current shuttle
     * @param _recievedToken Total wstETH amount recieved from the shuttle
     * @param _totalAmount Total wETH deposited into the shuttle
     * @return amount_ in wstETH
     */
    function calculateWstEthAmount(
        uint256 _balance,
        uint256 _recievedToken,
        uint256 _totalAmount
    ) public pure returns (uint256 amount_) {
        amount_ = (_balance.mul(_recievedToken)).div(_totalAmount);
    }

    /**
     * @dev This function allows users to claim their funds. There are three cases to it.
     *  1. Shuttle status is arrived: If shuttle is successfully processed and arrived. Users can claim wstEth token.
     *  2. If shuttle is marked as expired, then users can claim deposited wETH tokens.
     *  3. If shuttle is marked as cancelled, then users can claim deposited wETH tokens
     * @param _shuttleNumber Shuttle number for which the user wants to claim the token
     *
     */
    function _claim(uint256 _shuttleNumber) internal {
        Shuttle memory shuttle = shuttles[_shuttleNumber];
        ShuttleStatus status = shuttle.status;

        require(
            status == ShuttleStatus.ARRIVED ||
                status == ShuttleStatus.EXPIRED ||
                status == ShuttleStatus.CANCELLED,
            "!invalid shuttle status"
        );

        uint256 balance = balances[_shuttleNumber][msg.sender];
        balances[_shuttleNumber][msg.sender] = 0;

        address payable beneficiary = payable(msg.sender);

        require(balance > 0, "!amount");

        if (status == ShuttleStatus.ARRIVED) {
            // calculate wstEth Amount
            uint256 wstEthAmount = calculateWstEthAmount(
                balance,
                shuttles[_shuttleNumber].recievedToken,
                shuttles[_shuttleNumber].totalAmount
            );

            availableWstEthBalance = availableWstEthBalance.sub(
                wstEthAmount
            );
            wstEthToken.transfer(beneficiary, wstEthAmount);

            emit TokenClaimed(
                _shuttleNumber,
                address(wstEthToken),
                address(beneficiary),
                wstEthAmount
            );
        } else {
            availableWEthBalance = availableWEthBalance.sub(balance);
            wEthToken.transfer(beneficiary, balance);
            emit TokenClaimed(
                _shuttleNumber,
                address(wEthToken),
                address(beneficiary),
                balance
            );
        }
    }



    /**
     * @notice Claims the wstETH or wETH tokens
     * @dev Calls the internal _claim method
     * @param _shuttleNumber Shuttle number from which user wants to claim
    */
    function claim(uint256 _shuttleNumber) external nonReentrant whenNotPaused {
        _claim(_shuttleNumber);
    }


    /**
     * @notice Call if the expiration time has passed and the shuttle was not sent. Enables claims of wETH
     * @dev Calls _createNewShuttle which resets the shuttle ID
     * @param _shuttleNumber ID of the shuttle
     */
    function expireShuttle(uint256 _shuttleNumber) external whenNotPaused {
        require(
            _shuttleNumber == currentShuttle,
            "!only current shuttle allowed"
        );
        Shuttle memory shuttle = shuttles[_shuttleNumber];

        require(shuttle.totalAmount > 0, "!Not ready for expiry");
        require(block.number >= shuttle.expiry, "!not ready to expire");

        shuttles[_shuttleNumber].status = ShuttleStatus.EXPIRED;

        _createNewShuttle();
        emit ShuttleExpired(_shuttleNumber);
    }

    /**
     * @notice Operator can cancel the shuttle which is in available status, users will be able to claim deposited wETH tokens once shuttle is cancelled
     * @dev Calls _createNewShuttle which resets the shuttle ID
     * @param _shuttleNumber Id of shuttle.
     */
    function cancelShuttle(uint256 _shuttleNumber)
        external
        onlyRole(OPERATOR_ROLE)
    {
        require(
            _shuttleNumber == currentShuttle,
            "!only current shuttle allowed"
        );
        Shuttle memory shuttle = shuttles[_shuttleNumber];
        require(shuttle.status == ShuttleStatus.AVAILABLE, "!invalid status");

        shuttles[_shuttleNumber].status = ShuttleStatus.CANCELLED;
        _createNewShuttle();
        emit ShuttleCancelled(_shuttleNumber);
    }

    /** Setter */


    /**
     * @notice Set's the shuttle expiry block time
     * @param _shuttleExpiry shuttle expiry time in blocks
     */
    function setShuttleExpiry(uint256 _shuttleExpiry)
        external
        onlyRole(GOVERNANCE_ROLE)
    {
        require(_shuttleExpiry > 0, "Invalid shuttle expiry");

        shuttleExpiry = _shuttleExpiry;
        emit ShuttleExpiryChanged(shuttleExpiry);
    }


    receive() external payable {}

    fallback() external payable {}

}
