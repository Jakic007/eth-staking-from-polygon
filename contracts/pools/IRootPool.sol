// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.7;

contract IRootPool {


    event ShuttleProcessingInitiated(
        uint256 _shuttleNumber
    );
    event ShuttleProcessed(
        uint256 _shuttleNumber,
        uint256 _stakeAmount,
        uint256 _wstEthAmount,
        ShuttleProcessingStatus _processingStatus
    );
}

enum ShuttleProcessingStatus {
    PROCESSED
    }

/**
 * @dev Interface of a Root Tunnel contract used to send and receive messages through the bridge
*/
interface IFxStateRootTunnel {
    function receiveMessage(bytes memory message) external;

    /**
     * @dev Send the message to the child chain
     * @param message is a byte encoded message
    */
    function sendMessageToChild(bytes memory message) external;

    /**
     * @dev Reads the data about the shuttle received from the root chain
     * @return shuttleNumber shuttle number
     * @return amount amount of wETH in wei
    */
    function readData() external returns (uint256, uint256);
}

/**
 * @dev Interface of a Root Manager contract which is responsible for bridging tokens from and to Polygon PoS
*/
interface IRootManagerProxy {
    /**
     * @notice exit tokens by providing proof
     * @dev This function verifies if the transaction actually happened on child chain
     * the transaction log is then sent to token predicate to handle it accordingly
     *
     * @param inputData RLP encoded data of the reference tx containing following list of fields
     *  0 - headerNumber - Checkpoint header block number containing the reference tx
     *  1 - blockProof - Proof that the block header (in the child chain) is a leaf in the submitted merkle root
     *  2 - blockNumber - Block number containing the reference tx on child chain
     *  3 - blockTime - Reference tx block time
     *  4 - txRoot - Transactions root of block
     *  5 - receiptRoot - Receipts root of block
     *  6 - receipt - Receipt of the reference transaction
     *  7 - receiptProof - Merkle proof of the reference receipt
     *  8 - branchMask - 32 bits denoting the path of receipt in merkle tree
     *  9 - receiptLogIndex - Log Index to read from the receipt
     */
    function exit(bytes calldata inputData) external;
}

/**
 * @dev Interface of a Lido adapter contract used to abstract the staking and bridging functions
*/
interface ILidoAdapter {
    /**
     * @notice It performs staking to lido and bridges wstETH to
     *         Polygon PoS network.
     * @param _beneficiary Beneficiary address which will receive wstETH token.
     * @return Amount of wstETH that was sent to Polygon PoS
     */
    function depositForAndBridge(address _beneficiary)
        external
        payable
        returns (uint256);
}
