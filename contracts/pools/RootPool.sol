// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.7;

import {SafeMath} from "@openzeppelin/contracts/utils/math/SafeMath.sol";
import {IERC20} from "@openzeppelin/contracts/token/ERC20/IERC20.sol";

import "./IRootPool.sol";
import "./PoolSecurityModule.sol";

/**
 * @title Root Pool contract
 * @dev Processes shuttles received from the child chain and sends tokens back to the child chain
 */
contract RootPool is IRootPool, PoolSecurityModule {
    using SafeMath for uint256;

    IFxStateRootTunnel public rootTunnel;
    IRootManagerProxy public rootManagerProxy;
    ILidoAdapter public lidoAdapter;

    receive() external payable {}

    /**
     * @notice Initialize the contract and setup roles.
     * @param _rootTunnel - Address of the Root tunnel.
     * @param _rootManagerProxy - Address of Root Manager Proxy.
     * @param _lidoAdapter - Address of the Lido Adapter
     * @param _owner - Address of the owner
     */
    function initialize(
        IFxStateRootTunnel _rootTunnel,
        IRootManagerProxy _rootManagerProxy,
        ILidoAdapter _lidoAdapter,
        address _owner
    ) public initializer {
        __AccessControl_init();
        __Pausable_init();
        __ReentrancyGuard_init();

        rootTunnel = _rootTunnel;
        rootManagerProxy = _rootManagerProxy;
        lidoAdapter = _lidoAdapter;

        _setupRole(DEFAULT_ADMIN_ROLE, _owner);
        _setupRole(OPERATOR_ROLE, _owner);
        _setupRole(PAUSE_ROLE, _owner);
        _setupRole(GOVERNANCE_ROLE, _owner);
    }

    /**
     * @notice Perform stake and bridge funds back to the child chain
     * @dev This method performs cross chain staking.
     * It recieves message sent by child tunnel, claims burned token on polygon chain, stakes tokens to Lido and bridges them back to the child pool.
     * Proofs can be generated using Matic.js
     * @param _messageReceiveData Proof generated from the message event signature after checkpoint
     * @param _burnTokenData Proof generated from the transfer event signature after checkpoint
     */
    function crossChainStake(bytes memory _messageReceiveData, bytes memory _burnTokenData)
        external
        onlyRole(OPERATOR_ROLE)
        whenNotPaused
    {
        (uint256 shuttleNumber, uint256 amount) = _receieveShuttleFromChild(_messageReceiveData, _burnTokenData);

        // stake ETH and bridge wstEth
        uint256 wstEthAmount = lidoAdapter.depositForAndBridge{value: amount}(address(this));
        rootTunnel.sendMessageToChild(
            abi.encode(
                shuttleNumber,
                wstEthAmount,
                ShuttleProcessingStatus.PROCESSED
            )
        );

        emit ShuttleProcessed(
            shuttleNumber,
            amount,
            wstEthAmount,
            ShuttleProcessingStatus.PROCESSED
        );
    }

    /**
     * @dev Internal method to recieve shuttle which is initiated from child pool.
     * @param _messageReceiveData Proof generated from the message event signature after checkpoint
     * @param _burnTokenData Proof generated from the transfer event signature after checkpoint
     * @return shuttleNumber_ shuttle ID
     * @return amount_ amount in wei
     */
    function _receieveShuttleFromChild(bytes memory _messageReceiveData, bytes memory _burnTokenData) 
        internal
        returns(uint256 shuttleNumber_, uint256 amount_)
    {
        require(_messageReceiveData.length > 0, "!messageReceiveData");

        // recieve message send by child tunnel
        rootTunnel.receiveMessage(_messageReceiveData);

        // decode received message
        (shuttleNumber_, amount_) = rootTunnel.readData();

        // Step 2 for claiming tokens send from polygon to ethereum. Sometime, tokens are automatically transferred to beneficiary by 
        // bridge. Polygon bridge contracts maintains a queue. If a users claims tokens from brige, it will automatically transfer tokens for all the 
        // users who are ahead of this user.. 
        rootManagerProxy.exit(_burnTokenData);

        uint256 balance = address(this).balance;

        require(balance >= amount_, "Insufficient amount to stake");
        emit ShuttleProcessingInitiated(shuttleNumber_);
    }


    /** Setter */


    /**
     * @notice This will set address of lido adapter contract 
     * @param _lidoAdapter Address of lido adapter contract. 
     */
    function setLidoAdapter(ILidoAdapter _lidoAdapter)
        external
        onlyRole(GOVERNANCE_ROLE)
    {        
        lidoAdapter = _lidoAdapter;
    }

}
