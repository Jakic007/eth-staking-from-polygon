import { expect } from "chai";
import { ethers } from "hardhat";

describe("RootPool.init", function () {

    it('validate RootPool Deployment', async () => {

        const RootPool = await ethers.getContractFactory(
            "RootPool"
        );

        const [deployer, owner, rootTunnel, rootManagerProxy, erc20PredicateBurnOnly, depositManagerProxy, lidoAdapter] = await ethers.getSigners();

        const rootPool = await RootPool.connect(deployer).deploy();
        await rootPool.deployed();

        const initPool = await rootPool.connect(deployer).initialize(
            rootTunnel.address,
            rootManagerProxy.address,
            lidoAdapter.address,
            owner.address
        );

        // wait until the transaction is mined
        await initPool.wait();

        // assert storage
        expect(await rootPool.rootTunnel()).to.equal(rootTunnel.address);
        expect(await rootPool.rootManagerProxy()).to.equal(rootManagerProxy.address);
        expect(await rootPool.lidoAdapter()).to.equal(lidoAdapter.address);
        
        // validate roles owner has all the roles
        expect(await rootPool.hasRole(await rootPool.DEFAULT_ADMIN_ROLE(), owner.address)).to.equal(true);
        expect(await rootPool.hasRole(await rootPool.OPERATOR_ROLE(), owner.address)).to.equal(true);
        expect(await rootPool.hasRole(await rootPool.PAUSE_ROLE(), owner.address)).to.equal(true);
        expect(await rootPool.hasRole(await rootPool.GOVERNANCE_ROLE(), owner.address)).to.equal(true);

        // validate deployer has no roles
        expect(await rootPool.hasRole(await rootPool.DEFAULT_ADMIN_ROLE(), deployer.address)).to.equal(false);
        expect(await rootPool.hasRole(await rootPool.OPERATOR_ROLE(), deployer.address)).to.equal(false);
        expect(await rootPool.hasRole(await rootPool.PAUSE_ROLE(), deployer.address)).to.equal(false);
        expect(await rootPool.hasRole(await rootPool.GOVERNANCE_ROLE(), deployer.address)).to.equal(false);

    });

});