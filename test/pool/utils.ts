import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { ethers } from "hardhat";

export const deployChildPool = async (
    deployer: SignerWithAddress,
    shuttleExpiry: number,
    ownerAddress: string,
) => {

    const mockFxStateChildTunnel = await deployMockFxStateChildTunnel(deployer);
    const mockWEthToken = await deployMockERC20(deployer);
    const wstEthToken = await deployMockERC20(deployer);

    const ChildPool = await ethers.getContractFactory(
        "ChildPool"
    );
    const childPool = await ChildPool.connect(deployer).deploy();
    await childPool.deployed();


    const initPool = await childPool.connect(deployer).initialize(
        mockFxStateChildTunnel.address,
        mockWEthToken.address,
        wstEthToken.address,
        shuttleExpiry,
        ownerAddress,
    );
    await initPool.wait();


    return { childPool, mockWEthToken, mockFxStateChildTunnel, wstEthToken };
}

export const getShuttleInEnrouteState = async (
    deployer: SignerWithAddress,
    shuttleExpiry: number,
    owner: SignerWithAddress,
    user1: SignerWithAddress,
    user2: SignerWithAddress
) => {
    const { childPool, mockWEthToken, mockFxStateChildTunnel, wstEthToken } = await deployChildPool(deployer, shuttleExpiry, owner.address);

    let amount = ethers.utils.parseEther("1");

    await mockWEthToken.mint(user1.address, amount);
    await mockWEthToken.approveInternal(user1.address, childPool.address, amount);
    await childPool.connect(user1).deposit(amount);


    amount = ethers.utils.parseEther("2");
    await mockWEthToken.mint(user2.address, amount);
    await mockWEthToken.approveInternal(user2.address, childPool.address, amount);

    // deposit user 2
    await childPool.connect(user2).deposit(amount)

    await childPool.connect(owner).enrouteShuttle(1)

    return { childPool, mockWEthToken, mockFxStateChildTunnel, wstEthToken };
}

export const getShuttleInArrivedState = async (
    deployer: SignerWithAddress,
    shuttleExpiry: number,
    owner: SignerWithAddress,
    user1: SignerWithAddress,
    user2: SignerWithAddress,
    status: ShuttleProcessingStatus = ShuttleProcessingStatus.PROCESSED,
    depositAmount: string = "2"
) => {
    const { childPool, mockWEthToken, mockFxStateChildTunnel, wstEthToken } = await getShuttleInEnrouteState(deployer, shuttleExpiry, owner, user1, user2);

    const shuttleArrivalData = {
        wstEthAmount: ethers.utils.parseEther(depositAmount || "2"),
        shuttleProcessingStatus: status,
        shuttleNumber: 1
    }

    const encodedMessageData = ethers.utils.defaultAbiCoder.encode(
        ["uint256", "uint256", "uint8"],
        [shuttleArrivalData.shuttleNumber, shuttleArrivalData.wstEthAmount, shuttleArrivalData.shuttleProcessingStatus]
    );

    // mock message arrival 
    await mockFxStateChildTunnel.setLatestData(encodedMessageData);
    // mock token arrival 
    if (status == ShuttleProcessingStatus.PROCESSED) {
        await wstEthToken.mint(childPool.address, shuttleArrivalData.wstEthAmount);
    } 


    await childPool.connect(owner).arriveShuttle(1);

    return { childPool, mockWEthToken, mockFxStateChildTunnel, wstEthToken };

}

export const deployMockERC20 = async (deployer: SignerWithAddress) => {
    const MockERC20 = await ethers.getContractFactory(
        "MockERC20"
    );

    const mockERC20 = await MockERC20.connect(deployer).deploy(
        "MOCK", "MOCK", deployer.address, ethers.utils.parseEther("1000000")
    );
    return mockERC20;
}


export const deployMockFxStateChildTunnel = async (
    deployer: SignerWithAddress,
) => {
    const MockFxStateChildTunnel = await ethers.getContractFactory(
        "MockFxStateChildTunnel"
    );

    const mockFxStateChildTunnel = await MockFxStateChildTunnel.connect(deployer).deploy();
    return mockFxStateChildTunnel;
}

export enum ShuttleProcessingStatus {
    PROCESSED = 0,
    CANCELLED = 1
}

export enum ShuttleStatus {
    UNAVAILABLE = 0,
    AVAILABLE = 1,
    ENROUTE = 2,
    ARRIVED = 3,
    EXPIRED = 4,
    CANCELLED = 5
}