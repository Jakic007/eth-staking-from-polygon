import { expect } from "chai";
import { BigNumber } from "ethers";
import { ethers } from "hardhat";
import { advanceBlocks } from "../../testHelpers";
import { deployChildPool, getShuttleInArrivedState, getShuttleInEnrouteState, ShuttleProcessingStatus } from "../utils";

describe("ChildPool.claim", function () {

    it('should let user claim wstEth funds after shuttle in arrived', async() => {

        const [deployer, owner, user1, user2] = await ethers.getSigners();

        // deploy child pool
        const arrivedWstEth = '2';
        const { childPool , wstEthToken } = await getShuttleInArrivedState(
            deployer,
            2000,
            owner,
            user1,
            user2,
            ShuttleProcessingStatus.PROCESSED,
            arrivedWstEth
        );

        const wstEthAmount = BigNumber.from(ethers.utils.parseEther(arrivedWstEth));

        const shuttle = await childPool.shuttles(1);
        const totalAmount = BigNumber.from(shuttle.totalAmount);
        const recievedAmount = BigNumber.from(shuttle.recievedToken);

        const user1Deposit = BigNumber.from(ethers.utils.parseEther("1"));
        const user2Deposit = BigNumber.from(ethers.utils.parseEther("2"));

        const expectedWstEthAmountUser1 = user1Deposit.mul(recievedAmount).div(totalAmount);
        const expectedWstEthAmountUser2 = user2Deposit.mul(recievedAmount).div(totalAmount);
        
        let availableWstEthBalance = await childPool.availableWstEthBalance();
        const expectedWstEthBalance = ethers.utils.parseEther("2");
        expect(availableWstEthBalance).that.equals(expectedWstEthBalance);

        await expect(childPool.connect(user1).claim(1)).to.emit(childPool, 'TokenClaimed').withArgs(1, wstEthToken.address, user1.address, expectedWstEthAmountUser1);
        availableWstEthBalance = await childPool.availableWstEthBalance();
        expect(availableWstEthBalance).that.equals(expectedWstEthBalance.sub(BigNumber.from(expectedWstEthAmountUser1)));

        await expect(childPool.connect(user2).claim(1)).to.emit(childPool, 'TokenClaimed').withArgs(1, wstEthToken.address, user2.address, expectedWstEthAmountUser2);
        availableWstEthBalance = await childPool.availableWstEthBalance();
        expect(availableWstEthBalance).that.equals(expectedWstEthBalance.sub(BigNumber.from(expectedWstEthAmountUser1).add(BigNumber.from(expectedWstEthAmountUser2))));
    });


    it('should fail if shuttle is not in claimable state', async() => {
        const [deployer, owner, user1, user2] = await ethers.getSigners();

        // deploy child pool
        const { childPool, mockFxStateChildTunnel } = await getShuttleInEnrouteState(deployer, 2000, owner, user1, user2);

        await expect(childPool.connect(user1).claim(1)).to.be.revertedWith('!invalid shuttle status');
    });

    it('should fail if user doesnot have deposit in shuttle', async() => {
        const [deployer, owner, user1, user2] = await ethers.getSigners();

        // deploy child pool
        const { childPool } = await getShuttleInArrivedState(
            deployer,
            2000,
            owner,
            user1,
            user2,
            ShuttleProcessingStatus.PROCESSED,
            "2"
        );  

        await expect(childPool.connect(deployer).claim(1)).to.be.revertedWith('!amount');
    });

    it('user should not be able to claim twice', async() => {
        const [deployer, owner, user1, user2] = await ethers.getSigners();

        // deploy child pool
        const { childPool , wstEthToken } = await getShuttleInArrivedState(
            deployer,
            2000,
            owner,
            user1,
            user2,
            ShuttleProcessingStatus.PROCESSED,
            "2"
        ); 


        const shuttle = await childPool.shuttles(1);
        const totalAmount = BigNumber.from(shuttle.totalAmount);
        const recievedAmount = BigNumber.from(shuttle.recievedToken);

        const user1Deposit = BigNumber.from(ethers.utils.parseEther("1"));

        const expectedWstEthAmountUser1 = user1Deposit.mul(recievedAmount).div(totalAmount);

        await expect(childPool.connect(user1).claim(1)).to.emit(childPool, 'TokenClaimed').withArgs(1, wstEthToken.address, user1.address, expectedWstEthAmountUser1);
        await expect(childPool.connect(user1).claim(1)).to.be.revertedWith('!amount');        
    });

    it('should allow users to claim tokens after shuttle is expired', async() => {
        const [deployer, owner, user1] = await ethers.getSigners();

        // deploy child pool
        const expiryBlock = 5;
        const { childPool, mockWEthToken } = await deployChildPool(
            deployer,
            expiryBlock,
            owner.address
        );

        const amount = ethers.utils.parseEther("1");
        await mockWEthToken.mint(user1.address, amount);
        await mockWEthToken.approveInternal(user1.address, childPool.address, amount);    
        await childPool.connect(user1).deposit(amount);

        await advanceBlocks(10);

        await expect(childPool.connect(user1).expireShuttle(1)).to.emit(childPool, 'ShuttleExpired').withArgs(1);

        expect(childPool.connect(user1).claim(1)).to.emit(childPool, 'TokenClaimed').withArgs(1, mockWEthToken.address, user1.address, amount);

    });

    it('should allow users to claim tokens after shuttle is cancelled', async() => {
        const [deployer, owner, user1] = await ethers.getSigners();

        // deploy child pool
        const expiryBlock = 5;
        const { childPool, mockWEthToken } = await deployChildPool(
            deployer,
            expiryBlock,
            owner.address
        );

        const amount = ethers.utils.parseEther("1");
        await mockWEthToken.mint(user1.address, amount);
        await mockWEthToken.approveInternal(user1.address, childPool.address, amount);    
        await childPool.connect(user1).deposit(amount);

        await advanceBlocks(10);

        await expect(childPool.connect(owner).cancelShuttle(1)).to.emit(childPool, 'ShuttleCancelled').withArgs(1);

        expect(childPool.connect(user1).claim(1)).to.emit(childPool, 'TokenClaimed').withArgs(1, mockWEthToken.address, user1.address, amount);

    });

});