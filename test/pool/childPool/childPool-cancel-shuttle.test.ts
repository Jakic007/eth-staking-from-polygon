import { expect } from "chai";
import { ethers } from "hardhat";
import { advanceBlocks } from "../../testHelpers";
import { deployChildPool } from "../utils";

describe("ChildPool.cancelShuttle", function () {

    it('validate cancelShuttle ', async () => {

        const [deployer, owner, user1] = await ethers.getSigners();

        // deploy child pool
        const expiryBlock = 5;
        const { childPool, mockWEthToken } = await deployChildPool(
            deployer,
            expiryBlock,
            owner.address,
        );

        const amount = ethers.utils.parseEther("1");
        await mockWEthToken.mint(user1.address, amount);
        await mockWEthToken.approveInternal(user1.address, childPool.address, amount);    
        await childPool.connect(user1).deposit(amount);

        await expect(childPool.connect(owner).cancelShuttle(1)).to.emit(childPool, 'ShuttleCancelled').withArgs(1);
    });

    it('should revert if user with non operator role cancels it ', async () => {

        const [deployer, owner, user1] = await ethers.getSigners();

        // deploy child pool
        const expiryBlock = 5;
        const { childPool, mockWEthToken } = await deployChildPool(
            deployer,
            expiryBlock,
            owner.address
        );

        const amount = ethers.utils.parseEther("1");
        await mockWEthToken.mint(user1.address, amount);
        await mockWEthToken.approveInternal(user1.address, childPool.address, amount);    
        await childPool.connect(user1).deposit(amount);

        await expect(childPool.connect(user1).cancelShuttle(1)).to.be.revertedWith(`AccessControl: account ${user1.address.toLowerCase()} is missing role 0x97667070c54ef182b0f5858b034beac1b6f3089aa2d3188bb1e8929f4fa9b929`);
    });

    it('should allow deposit of funds to next shuttle if current shuttle is cancelled ', async() => {

        const [deployer, owner, user1] = await ethers.getSigners();

        // deploy child pool
        const expiryBlock = 5;
        const { childPool, mockWEthToken } = await deployChildPool(
            deployer,
            expiryBlock,
            owner.address
        );

        const amount = ethers.utils.parseEther("1");
        await mockWEthToken.mint(user1.address, amount);
        await mockWEthToken.approveInternal(user1.address, childPool.address, amount);    
        await childPool.connect(user1).deposit(amount);

        await advanceBlocks(10);
        await expect(childPool.connect(owner).cancelShuttle(1)).to.emit(childPool, 'ShuttleCancelled').withArgs(1);

        await mockWEthToken.mint(user1.address, amount);
        await mockWEthToken.approveInternal(user1.address, childPool.address, amount);    
        await expect(childPool.connect(user1).deposit(amount)).to.emit(childPool, 'Deposit').withArgs(2, user1.address, amount);;
    })

});