import { expect } from "chai";
import { ethers } from "hardhat";
import { advanceBlocks } from "../../testHelpers";
import { deployChildPool } from "../utils";

describe("ChildPool.expireShuttle", function () {

    it('validate expireShuttle if current block number is greater than expiry block ', async () => {

        const [deployer, owner, user1] = await ethers.getSigners();

        // deploy child pool
        const expiryBlock = 5;
        const { childPool, mockWEthToken } = await deployChildPool(
            deployer,
            expiryBlock,
            owner.address
        );

        const amount = ethers.utils.parseEther("1");
        await mockWEthToken.mint(user1.address, amount);
        await mockWEthToken.approveInternal(user1.address, childPool.address, amount);    
        await childPool.connect(user1).deposit(amount);

        await advanceBlocks(10);

        await expect(childPool.connect(user1).expireShuttle(1)).to.emit(childPool, 'ShuttleExpired').withArgs(1);
    });

    it('validate expireShuttle if current block number is equal to expiry block ', async () => {

        const [deployer, owner, user1] = await ethers.getSigners();

        // deploy child pool
        const expiryBlock = 5;
        const { childPool, mockWEthToken } = await deployChildPool(
            deployer,
            expiryBlock,
            owner.address
        );

        const amount = ethers.utils.parseEther("1");
        await mockWEthToken.mint(user1.address, amount);
        await mockWEthToken.approveInternal(user1.address, childPool.address, amount);    
        await childPool.connect(user1).deposit(amount);

        await advanceBlocks(expiryBlock);

        await expect(childPool.connect(user1).expireShuttle(1)).to.emit(childPool, 'ShuttleExpired').withArgs(1);
    });

    it('should revert if expiry block is less than current block ', async () => {

        const [deployer, owner, user1] = await ethers.getSigners();

        // deploy child pool
        const expiryBlock = 10;
        const { childPool, mockWEthToken } = await deployChildPool(
            deployer,
            expiryBlock,
            owner.address
        );

        const amount = ethers.utils.parseEther("1");
        await mockWEthToken.mint(user1.address, amount);
        await mockWEthToken.approveInternal(user1.address, childPool.address, amount);    
        await childPool.connect(user1).deposit(amount);
        await advanceBlocks(2);

        await expect(childPool.connect(user1).expireShuttle(1)).to.be.revertedWith("!not ready to expire");
    });

    it('should revert if shuttle status is not AVAILABLE ', async () => {

        const [deployer, owner, user1] = await ethers.getSigners();

        // deploy child pool
        const expiryBlock = 5;
        const { childPool } = await deployChildPool(
            deployer,
            expiryBlock,
            owner.address
        );


        const wrongShuttleId = 10;
        await expect(childPool.connect(user1).expireShuttle(wrongShuttleId)).to.be.revertedWith("!only current shuttle allowed");
    });

    it('should allow deposit of funds to next shuttle if current shuttle is expired ', async() => {

        const [deployer, owner, user1] = await ethers.getSigners();

        // deploy child pool
        const expiryBlock = 5;
        const { childPool, mockWEthToken } = await deployChildPool(
            deployer,
            expiryBlock,
            owner.address
        );

        const amount = ethers.utils.parseEther("1");
        await mockWEthToken.mint(user1.address, amount);
        await mockWEthToken.approveInternal(user1.address, childPool.address, amount);    
        await childPool.connect(user1).deposit(amount);

        await advanceBlocks(10);

        await expect(childPool.connect(user1).expireShuttle(1)).to.emit(childPool, 'ShuttleExpired').withArgs(1);

        await mockWEthToken.mint(user1.address, amount);
        await mockWEthToken.approveInternal(user1.address, childPool.address, amount);    
        await expect(childPool.connect(user1).deposit(amount)).to.emit(childPool, 'Deposit').withArgs(2, user1.address, amount);;
    })

});