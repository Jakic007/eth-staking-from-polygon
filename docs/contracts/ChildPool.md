# Korisnička Dokumentacija za Child Pool

## Pregled

Pametni ugovor `ChildPool` upravlja korisničkim depozitima i zahtjevima na lancu djetetu. Ovaj ugovor omogućuje korisnicima da pošalju wETH tokene u trenutni shuttle, te pruža mehanizme za obradu, slanje i finalizaciju shuttle transakcija preko child tunela. Ugovor je dizajniran s fokusom na sigurnost, korištenjem modula `PoolSecurityModule` i OpenZeppelin-ovih sigurnosnih biblioteka.

## Ključne Komponente

- **IFxStateChildTunnel**: Sučelje za komunikaciju sa root chainom.
- **IWEthToken & IERC20**: Sučelja za interakciju s wETH i wstETH tokenima.
- **Shuttle**: Struktura koja predstavlja pojedinačni shuttle, sadrži informacije kao što su ukupna količina, status, primljeni tokeni i rok trajanja.

## Inicijalizacija

- **initialize(IFxStateChildTunnel _childTunnel, IWEthToken _wEthToken, IERC20 _wstEthToken, uint256 _shuttleExpiry, address _owner)**: Postavlja početne vrijednosti ugovora, uključujući adrese tunela i tokena, vrijeme isteka shuttlea, i vlasnika.

## Glavne Funkcije

- **deposit(uint256 amount)**: Omogućuje korisnicima da pošalju wETH tokene u trenutni shuttle.
- **enrouteShuttle(uint256 _shuttleNumber)**: Operater može poslati trenutni shuttle prema Ethereumu, pokrećući prenos sredstava.
- **arriveShuttle(uint256 _shuttleNumber)**: Finalizira shuttle nakon primanja sredstava i poruka s korjenskog lanca.
- **claim(uint256 _shuttleNumber)**: Korisnici mogu zahtijevati svoje tokene nakon što je shuttle finaliziran.
- **expireShuttle(uint256 _shuttleNumber)**: Omogućuje istek shuttlea ako nije poslan prije definiranog roka.
- **cancelShuttle(uint256 _shuttleNumber)**: Operater može otkazati shuttle koji je u dostupnom statusu, omogućujući korisnicima da zahtijevaju povrat deponiranih wETH tokena.
- **setShuttleExpiry(uint256 _shuttleExpiry)**: Postavlja vrijeme isteka shuttlea u blokovima.

## Dogadjaji (Events)

- **ShuttleCreated**: Emitira se kada je stvoren novi shuttle.
- **Deposit**: Emitira se kada korisnik pošalje tokene u shuttle.
- **ShuttleEnrouted**: Emitira se kada je shuttle poslan.
- **ShuttleArrived**: Emitira se kada shuttle stigne i sredstva su primljena.
- **TokenClaimed**: Emitira se kada korisnik preuzme svoje tokene.
- **ShuttleExpired**: Emitira se kada istekne vrijeme shuttlea.
- **ShuttleCancelled**: Emitira se kada je shuttle otkazan.
- **ShuttleExpiryChanged**: Emitira se kada je postavljeno novo vrijeme isteka shuttlea.

## Sigurnosni Moduli

- **ReentrancyGuard**: Sprječava reentrancy napade.
- **Pausable**: Omogućuje pauziranje ugovora u hitnim situacijama.
- **AccessControl**: Upravlja pristupom različitim funkcijama ugovora.

## Kako Koristiti

1. **Inicijalizacija**: Prije korištenja, ugovor mora biti inicijaliziran s potrebnim parametrima i adresama.
2. **Depozit**: Korisnici mogu poslati wETH tokene pozivom funkcije `deposit`.
3. **Shuttle Upravljanje**: Operater može upravljati shuttleima, uključujući njihovo slanje, finalizaciju, otkazivanje ili označavanje kao istekle.
4. **Zahtjevanje Tokena**: Nakon finalizacije shuttlea, korisnici mogu preuzeti svoje wstETH ili wETH tokene, ovisno o statusu shuttlea.

## Napomene

- Ovaj pametni ugovor koristi OpenZeppelin biblioteke za dodatnu sigurnost i standardizaciju.
- Korisnici i operateri trebaju biti svjesni trenutnog shuttlea i njegovog statusa prije izvođenja operacija.
- Sigurnosne mjere kao što su `nonReentrant` i `whenNotPaused` osiguravaju stabilnost i sigurnost ugovora.

