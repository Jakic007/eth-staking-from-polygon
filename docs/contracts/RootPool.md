# Korisnička Dokumentacija za Root Pool

## Pregled

Pametni ugovor `Root Pool` obrađuje shuttle transakcije primljene s child chaina i šalje tokene natrag na child chain. Ovaj ugovor omogućava staking ETH preko Lido adaptera i bridging wstETH tokena natrag na child pool.

## Ključne Komponente

- **IFxStateRootTunnel**: Sučelje za komunikaciju s child chainom.
- **IRootManagerProxy**: Sučelje za upravljanje root managerom.
- **ILidoAdapter**: Sučelje za Lido staking adapter.

## Inicijalizacija

- **initialize(IFxStateRootTunnel _rootTunnel, IRootManagerProxy _rootManagerProxy, ILidoAdapter _lidoAdapter, address _owner)**: Postavlja početne vrijednosti ugovora, uključujući adrese tunela, root manager proxy, Lido adapter, i vlasnika.

## Glavne Funkcije

- **crossChainStake(bytes memory _messageReceiveData, bytes memory _burnTokenData)**: Izvršava cross-chain staking. Prima poruku poslanu s child tunela, potvrđuje izgorene tokene na Polygon lanacu, stavlja tokene u stake kroz Lido protokol i bridgea wstETH na child pool.

## Interna Metodika

- **_receieveShuttleFromChild(bytes memory _messageReceiveData, bytes memory _burnTokenData)**: Interna metoda za primanje shuttlea koji je pokrenut s child poola. Ova metoda dekodira i obrađuje primljene podatke, potvrđuje tokene poslane s Polygon na Ethereum, i provjerava iznos za staking.

## Setters

- **setLidoAdapter(ILidoAdapter _lidoAdapter)**: Postavlja adresu Lido adapter ugovora.

## Događaji (Events)

- **ShuttleProcessed**: Emitira se nakon što je shuttle procesiran.
- **ShuttleProcessingInitiated**: Emitira se kada započne obrada shuttlea.

## Kako Koristiti

1. **Inicijalizacija**: Inicijalizirajte ugovor s potrebnim parametrima i adresama. Samo jednom potrebno izvršiti.
2. **Cross-Chain Stake**: Da biste izvršili staking i bridging, operater mora pozvati `crossChainStake` s potrebnim dokazima i podacima.
3. **Postavljanje Lido Adaptera**: Governance može postaviti adresu Lido adaptera pozivom `setLidoAdapter`.

## Napomene

- Zahtijeva da operatori i governance imaju odgovarajuće role za izvršenje funkcija.
- Korištenje ovog ugovora zahtijeva poznavanje procesa bridginga i stakinga preko Lido adaptera.
