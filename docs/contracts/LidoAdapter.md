# Korisnička Dokumentacija za Lido Adapter

## Pregled

Pametni ugovor `LidoAdapter` služi kao omotač za staking kroz Lido protokol i bridging wstETH tokena na Polygon PoS mrežu. 
## Glavne Karakteristike

- **Staking i Bridging**: Omogućuje staking ETH-a kroz Lido i bridging wstETH tokena na Polygon PoS mrežu.
- **Sigurnost**: Koristi OpenZeppelin-ove sigurnosne biblioteke za sigurnu interakciju s tokenima.

## Funkcije

### depositForAndBridge

```solidity
function depositForAndBridge(address _beneficiary) external payable returns (uint256)
```

#### Opis

Izvršava staking poslanog ETH-a kroz Lido i bridgea nastali wstETH token na Polygon PoS mrežu za određenog korisnika (_beneficiary).

#### Parametri

- `_beneficiary`: Adresa korisnika koja će primiti wstETH token na Polygon PoS mreži.

#### Povratne Informacije

- Vraća količinu wstETH tokena koja je bridgana na Polygon PoS mrežu.

## Interna Logika

### _bridgeToMatic

```solidity
function _bridgeToMatic(uint256 _wstTokenAmount, address _beneficiary) private
```

#### Opis

Privatna funkcija koja bridgea zadanu količinu wstETH tokena (_wstTokenAmount) na Polygon PoS mrežu za određenog korisnika (_beneficiary).

#### Parametri

- `_wstTokenAmount`: Količina wstETH tokena koja se bridga.
- `_beneficiary`: Adresa korisnika koja će primiti wstETH token na Polygon PoS mreži.

### _stake

```solidity
function _stake() private returns (uint256)
```

#### Opis

Privatna funkcija koja izvršava stake poslanog ETHa kroz Lido i vraća količinu dobivenih wstETH tokena.

#### Povratne Informacije

- Vraća količinu wstETH tokena dobivenu stakingom.

## Događaji (Events)

### Deposited

```solidity
event Deposited(address _beneficiary, uint256 _eth, uint256 _wstEth, bool _isBridged);
```

#### Opis

Emitira se nakon uspješnog stakinga i bridginga, označavajući količinu ETH-a koja je stavljena u stake, količinu wstETH tokena koja je bridgana, i status bridginga.

#### Parametri

- `_beneficiary`: Adresa korisnika koja prima wstETH token.
- `_eth`: Količina ETH-a koja je stavljena u stake.
- `_wstEth`: Količina wstETH tokena koja je bridgeana.
- `_isBridged`: Status da li je bridging uspješno izvršen.

## Napomene

- Ovaj ugovor zahtijeva da korisnici šalju ETH koji će biti automatski stavljen u stake kroz Lido protokol.
- Bridging zahtijeva interakciju s vanjskim ugovorima i mrežama, te se korisnici trebaju upoznati s pripadajućim rizicima i mehanizmima sigurnosti.
```
Ova dokumentacija detaljno opisuje funkcionalnosti, procese i sigurnosne mehanizme `LidoAdapter` pametnog ugovora, pružajući korisnicima i developerima sve potrebne informacije za njegovu upotrebu.