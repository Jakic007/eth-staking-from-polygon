## Upute za Korištenje za Krajnjeg Korisnika

Ove upute omogućavaju krajnjim korisnicima da šalju wETH tokene na Child chain i kasnije pozivaju `claim` na istom chainu. Svi procesi u pozadini, uključujući staking, bridging i upravljanje shuttleima, apstrahirani su od korisnika.

### Slanje wETH Tokena na Child Chain

Da biste poslali wETH tokene na Child chain, slijedite ove korake:

1. **Depozit wETH Tokena:** Da biste izvršili depozit, koristite funkciju `deposit` u ChildPool ugovoru. Potrebno je specificirati količinu wETH tokena koje želite poslati. Ovaj korak zahtijeva interakciju s pametnim ugovorom putem odgovarajućeg sučelja na blockchainu (npr., kroz web aplikaciju ili direktno preko blockchain walleta).

   ```solidity
   function deposit(uint256 amount) external;
   ```

   Primjer kako to možete učiniti putem web aplikacije ili walleta koji podržava interakciju sa smart ugovorima:

   - Otvorite aplikaciju/wallet.
   - Navigirajte do opcije za slanje transakcije.
   - Unesite adresu ChildPool ugovora.
   - Pozovite funkciju `deposit` s količinom wETH koju želite poslati.

2. **Čekanje na Obradu:** Nakon što izvršite depozit, vaši tokeni će biti uključeni u shuttle koji će ih prenijeti na Ethereum mainnet i staviti u stake preko Lido adaptera. Ovaj proces može potrajati neko vrijeme, ovisno o dinamici mreže i shuttle rasporedu.

### Pozivanje Claim na Child Chainu

Nakon što je vaš depozit obraden i tokeni su stavljeni u stake, moći ćete pozvati `claim` na Child chainu kako biste preuzeli svoje wstETH tokene.

1. **Poziv funkcije Claim:** Da biste preuzeli svoje tokene, koristite funkciju `claim` u ChildPool ugovoru. Potrebno je specificirati broj shuttlea iz kojeg želite preuzeti tokene.

   ```solidity
   function claim(uint256 _shuttleNumber) external;
   ```

   Kako pozvati `claim`:

   - Otvorite aplikaciju/wallet.
   - Navigirajte do opcije za slanje transakcije.
   - Unesite adresu ChildPool ugovora.
   - Pozovite funkciju `claim` s brojem shuttlea za koji očekujete tokene.

2. **Preuzimanje Tokena:** Nakon uspješnog poziva `claim` funkcije, wstETH tokeni će biti preneseni na vašu adresu na Child chainu.

### Napomene

- Ako niste sigurni u broj shuttlea za vaš depozit, možete provjeriti događaje (events) emitirane od strane ChildPool ugovora ili se obratiti podršci platforme koju koristite za interakciju s pametnim ugovorima.
- Sve transakcije na blockchainu zahtijevaju plaćanje gasa. Osigurajte da imate dovoljno native tokena na chainu na kojem izvršavate transakciju za pokrivanje troškova gasa.
- Upute se mogu razlikovati ovisno o korištenoj aplikaciji ili walletu za interakciju sa smart ugovorima.
```
Ove upute pružaju jasan i sažet pregled koraka koje krajnji korisnici trebaju poduzeti kako bi poslali wETH tokene na Child chain i kasnije pozvali `claim` za svoje tokene.
