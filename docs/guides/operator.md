## Upute za Operatora za Procesuiranje Shuttlea

Kao operator, vaša je uloga ključna u procesu slanja akumuliranih wETH tokena s Child chaina na Root chain i daljnje obrade. Slijedite ove korake kako biste uspješno procesuirali shuttle.

### Slanje Shuttlea s Child na Root Chain

1. **Provjera Spremnosti Shuttlea:** Prije slanja shuttlea, provjerite je li zadovoljavajuća količina wETH tokena akumulirana na Child chainu. To možete učiniti pregledom stanja ChildPool ugovora i shuttlea koji su spremni za slanje. 

2. **Slanje Shuttlea:** Kada se utvrdi da je shuttle spreman, koristite funkciju `enrouteShuttle` u ChildPool ugovoru za slanje shuttlea na Root chain. Ova funkcija prenosi wETH tokene u odgovarajući shuttle i označava ga kao "enroute".

   ```solidity
   function enrouteShuttle(uint256 _shuttleNumber) external;
   ```

   - Ova funkcija zahtijeva da ste autentificirani kao operator i da je shuttle u statusu koji omogućuje slanje.
   - Shuttle se označava kao "enroute" što znači da je u procesu slanja na Root chain.

### Obrada Shuttlea na Root Chainu

Nakon što je shuttle poslan na Root chain, slijedeći koraci se odnose na njegovu obradu na Root chainu.

1. **Prijem Shuttlea:** Kao operator, trebate pratiti dolazak shuttlea na Root chain. To uključuje praćenje poruka koje dolaze preko RootTunnel ugovora.

2. **Staking i Bridging:** Jednom kada shuttle stigne na Root chain, potrebno je izvršiti staking prikupljenih wETH tokena preko Lido adaptera i bridging nastalih wstETH tokena natrag na Child chain. Ovo se postiže pozivanjem funkcije `crossChainStake` u RootPool ugovoru.

   ```solidity
   function crossChainStake(bytes memory _messageReceiveData, bytes memory _burnTokenData) external;
   ```

   - `_messageReceiveData` i `_burnTokenData` su dokazi potrebni za verifikaciju transakcije i obradu stakinga. Za generiranje dokaza, postoji skripte za čije
   su korištenje napisane upute u nastavku ovog dokumenta.
   - Ova funkcija obrađuje shuttle, stavlja wETH tokene u stake preko Lido adaptera, i bridgea nastale wstETH tokene natrag na Child chain.

### Završetak Procesa

Nakon uspješnog stakinga i bridginga:

- Shuttle se označava kao obrađen, a odgovarajući wstETH tokeni se šalju natrag na Child chain za korisnike koji su sudjelovali u shuttleu.
- Kao operator, važno je osigurati da se svi koraci pravilno dokumentiraju i prate kako bi se osigurao transparentan i siguran proces.

### Napomene

- Operateri moraju imati odgovarajuće ovlasti (ROLE) za izvršavanje funkcija ugovora.
- Svi koraci zahtijevaju pažljivo praćenje i upravljanje kako bi se osigurala sigurnost i efikasnost procesa.
- Komunikacija između Child i Root chaina koristi složene mehanizme poput tunela, što zahtijeva razumijevanje kako ti mehanizmi funkcioniraju.


### Generiranje Proofova za Token Burn i Message Transfer

Kao operator, važno je generirati dokaze za token burn i message transfer s Child chaina kako biste mogli pozvati funkciju `crossChainStake` na Root chainu. Proofovi se mogu generirati korištenjem skripte `proof_generation.js`.

#### Koraci za Generiranje Proofova

1. **Priprema:** Prvo, osigurajte da imate instalirane potrebne pakete za pokretanje skripte. Skripta koristi `@maticnetwork/maticjs`, `@maticnetwork/maticjs-web3`, i druge povezane pakete.

2. **Konfiguracija Skripte:** Uredite skriptu `proof_generation.js` kako biste postavili relevantne konfiguracije za vaše mreže. Potrebno je specificirati pristupne točke (provider URLs) za Ethereum (parent) i Polygon (child) mreže.

3. **Specifikacija Dogadjaja:** Unesite potpis događaja transfera ili poruke (TRANSFER_EVENT_SIG) koji je potreban za generiranje dokaza. Ovo je heksadecimalni string koji predstavlja potpis Ethereum događaja.

4. **Unos Polygon TX Hasha:** Unesite hash transakcije na Polygonu (BURN_TX_HASH) koja predstavlja withdrawal ili burn akciju za koju trebate generirati dokaz.

5. **Pokretanje Skripte:** Pokrenite skriptu `proof_generation.js` kako biste generirali dokaze. Skripta će ispisati potrebne informacije koje trebate za poziv funkcije na Root chainu.

    ```bash
    node proof_generation.js
    ```


#### Napomene

- Skripta `proof_generation.js` omogućava fleksibilno generiranje dokaza za različite tipove transakcija, uključujući one s višestrukim token transferima (ovdje to nije potrebno).
- Važno je detaljno pratiti sve korake i provjeriti valjanost svih unesenih informacija kako bi se osigurao uspješan proces generiranja i korištenja dokaza.
- Pristupne točke (provider URLs) trebaju biti ažurirane prema vašim specifičnim konfiguracijama i pristupnim ključevima.



