# Usluga grupne validacije Ethereum mreže korištenjem tehnologija za skaliranje mreža lanca blokova


## Zašto je ovaj projekt koristan?

Ethereum mreža je najpopularnija blockchain mreža, no uz visok broj transakcija ne skalira, te krajnji korisnici moraju platiti veliku količinu ETHa kako bi njihove transakcije bile uključene u blok.
Trenutno najpopularniji način za zaobilaženje ovih ograničenja je korištenje bočnih lanaca (sidechain) ili kompresijskih slojeva (rollupa).
Ovaj projekt je prilagođen za Polygon PoS koji je sidechain Ethereumu, ali uz modifikacije dijela koda zaslužnog za bridging, moguće je napraviti sustav koji radi primjerice s Arbitrumom.
Korisnik Polygon PoS mreže koji posjeduje wETH token može poslati svoje tokene u ovaj protokol kako bi stakeao svoj wETH u Lido.
Nakon što protokol prikupi depozite od ostalih korisnika, on će procesuirati shuttle i svi korisnici će moći preuzeti svoje wstETH tokene na Polygon PoS mreži.
Ukoliko bi korisnik ručno htjeo napraviti tu radnju trebao bi proći kroz ove korake:
1. Započni prebacivanje wETHa na Ethereum
2. Preuzmi ETH na Ethereumu 
3. Pošalji ETH u Lido i primi stETH
4. Dozvoli wstETH ugovoru da koristi stETH
5. Omotaj stETH u wstETH
6. Dozvoli Polygon bridge ugovoru da koristi wstETH
7. Pošalji wstETH na Polygon

Za korisnika to znači da bi trebao platiti 7 transakcija od kojih su 6 na Ethereumu. Ovisno o trenutnoj utilizaciji mreže i cijeni Ethera, korisnik bi mogao platiti i do 200 EUR za izvršavanje ovog slijeda što je neisplativo za većinu korisnika.

Koristeći grupnu validaciju i ovaj protokol to izgleda ovako:
1. Dozvoli Child Pool ugovoru da koristi wETH
2. Pošalji wETH u Child Pool 
3. Preuzmi wstETH na Polygon PoS mreži iz Child Poola

Broj transakcija se smanjio sa 7 na 3. Ono što je još važnije je da su sve 3 transakcije na Polygonu tako da korisnik može očekivati trošak od svega nekoliko desetaka centi. Osim smanjenja troška, značajno je olakšan proces za korisnika i od njega se ne zahtjeva duboko razumijevanje nekoliko različitih protokola da bi mogao prisustvovati decentralizaciji i sigurnosti Ethereuma. 



## Koraci za Deployment

Prije pokretanja ovih naredbi potrebno je instalirati module i postaviti varijable okruženja te prevesti i testirati kod.

### Pretkoraci
1. `npm run install`
2. `cp .env.example .env`
3. U `.env` datoteci postaviti tražene varijable
4. `npx hardhat compile` za prevođenje koda
5. `npm run test` za pokretanje testova prije deploymenta


### Deployment
1. Deploy Tunela:

   ```bash
   npm run deploy-tunnel-goerli
   npm run deploy-tunnel-mumbai
   ```

2. Postavi FxRoot na Child Tunnel i FxChild na Root Tunnel.

3. Deploy Lido adaptera koristeći `npm run deploy-adapter-goerli`.

4. Deploy bazena (pool), pazite da koristite ključ s istim nonceom. Oba bazena trebaju imati istu adresu.

   ```bash
   npm run deploy-pool-goerli
   npm run deploy-pool-mumbai
   ```

5. Postavi adresu RootPool-a na rootTunnel.

6. Postavi adresu ChildPool-a na childTunnel.

7. Inicijaliziraj RootPool i ChildPool.

Ovi koraci pružaju detaljne upute za deployment pametnih ugovora, uključujući postavljanje tunela, deploy Lido adaptera, i inicijalizaciju bazena na Goerli i Mumbai mrežama.




