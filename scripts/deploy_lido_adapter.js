const { deployContract } = require("ethereum-waffle");
const { ethers, upgrades, hre } = require("hardhat");

async function main() {
  const LidoAdapter = await ethers.getContractFactory("LidoAdapter");
  const lidoAdapter = await LidoAdapter.deploy();
  await lidoAdapter.deployed();
  console.log("Lido Adapter deployed to:", lidoAdapter.address);
}

main()
  .then(() => {
    console.log("Done");
  })
  .catch((error) => {
    console.error(error);
  });
