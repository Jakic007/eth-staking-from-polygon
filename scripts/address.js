const addresses = {
  1: {
    checkPointManager: "0x86E4Dc95c7FBdBf52e33D563BbDB00823894C287",
    fxRoot: "0xfe5e5D361b2ad62c541bAb87C45a0B9B018389a2",
    rootTunnel: "",
    rootManagerProxy: "0x2A88696e0fFA76bAA1338F2C74497cC013495922",
    etherPredicateProxy: "",
    depositManagerProxy: "0x401F6c983eA34274ec46f84D70b31C151321188b",
    lidoAdapter: "",
    rootPoolOwner: "",
    rootPoolProxy: "",
  },
  5: {
    checkPointManager: "0x2890bA17EfE978480615e330ecB65333b880928e",
    fxRoot: "0x3d1d3E34f7fB6D26245E6640E1c50710eFFf15bA",
    rootTunnel: "0xf9CE62874D1e31f6a9a0884eaDfd486d7E0A7075",
    rootManagerProxy: "0xBbD7cBFA79faee899Eaf900F13C9065bF03B1A74", 
    etherPredicateProxy: "0xe2B01f3978c03D6DdA5aE36b2f3Ac0d66C54a6D5",
    depositManagerProxy: "0xBbD7cBFA79faee899Eaf900F13C9065bF03B1A74",
    lidoAdapter: "0x3a9B88aBCA123447AdD846B34388eFcbF90D37f2",
    rootPoolOwner: "0x1F0d32e5A1E4748E061AcB5CBeE87b2243778e1E",
    rootPool: "0xDc911Cd802B8381F0Ad88282036D245f66F85A10",
  },
  137: {
    fxChild: "0x8397259c983751DAf40400790063935a11afa28a",
    childTunnel: "",
    wETHTokenOnChild: "",
    wstEthToken: "",
    shuttleExpiry: 302400,
    childPoolOwner: "",
    childPoolProxy: "",
  },
  80001: {
    fxChild: "0xCf73231F28B7331BBe3124B907840A94851f9f11",
    childTunnel: "0xeB0F6d99782C2b91b4bE0999F4F181CBA60dE3f7",
    wETHTokenOnChild: "0xA6FA4fB5f76172d178d61B04b0ecd319C5d1C0aa",
    wstEthToken: "0xF9a4BBAa7fA1DD2352F1A47d6d3fcfF259A6D05F",
    shuttleExpiry: 10,
    childPoolOwner: "0x1F0d32e5A1E4748E061AcB5CBeE87b2243778e1E",
    childPool: "0xDc911Cd802B8381F0Ad88282036D245f66F85A10", 
  },
};

module.exports = addresses;
