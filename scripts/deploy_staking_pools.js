const { ethers, upgrades } = require("hardhat");
const hre = require("hardhat");
const { BigNumber } = require("@ethersproject/bignumber");
const addresses = require("./address.js");

console.log("hre.network.name ", hre.network.name);

const deployRoot =
  hre.network.name === "goerli" || hre.network.name === "mainnet";

async function main() {
  const [deployer] = await ethers.getSigners();
  const deployerAddress = deployer.address;
  console.log(`\n\n\n Deployer Address: ${deployerAddress} \n\n\n`);
  const balanceBefore = await hre.web3.eth.getBalance(deployerAddress);
  console.log("before balance of deployer ", balanceBefore.toString());

  const chainId = await hre.web3.eth.getChainId();
  console.log({ chainId });
  console.log({ add: addresses[chainId] });

  // Deploy rootPool
  if (deployRoot) {
    const RootPool = await ethers.getContractFactory("RootPool");

    const rootPool = await RootPool.deploy();
    await rootPool.deployed();
    console.log("RootPool deployed to:", rootPool.address);
  }

  // Deploy childPool
  if (!deployRoot) {
    
    const ChildPool = await ethers.getContractFactory("ChildPool");

    const childPool = await ChildPool.deploy();
    await childPool.deployed();
    console.log("childPool deployed to:", childPool.address);
  }

  const balanceAfter = await hre.web3.eth.getBalance(deployerAddress);
  console.log("after balance of deployer ", balanceAfter.toString());
  console.log(
    "Total deployment cost  ",
    ethers.utils.formatUnits(
      BigNumber.from(balanceBefore).sub(BigNumber.from(balanceAfter))
    )
  );
}

main().then(() => {
  console.log("Done");
});
