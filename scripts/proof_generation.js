/*

You may need to manually withdraw from the Polygon blockchain.

Once you've run the withdraw transaction on Polygon, you'll need to provide a burn proof on Ethereum.

Use the script below to generate the proof, then go to the WithdrawToWrapperRoot contract on Etherscan:

https://etherscan.io/address/0xb67a8438e15918de8f42aab84eb3950f2588a534

paste in the burn proof and enter '1' for the offset
*/

// You'll need the following packages
const matic = require("@maticnetwork/maticjs")
const { Web3ClientPlugin } = require("@maticnetwork/maticjs-web3")

const { ExitUtil, Web3SideChainClient, RootChain, ABIManager } = matic
matic.use(Web3ClientPlugin)

const config = {
    network: 'mainnet',
    version: 'v1',
    parent: {
        provider: "https://eth-goerli.g.alchemy.com/v2/xxx"
    },
    child: {
        provider: "https://polygon-mumbai.g.alchemy.com/v2/xxx"
    },
}

// Set message or token transfer signature depending on the need for a proof
const TRANSFER_EVENT_SIG = '0x...'

// Enter the Polygon tx hash of the withdrawal
const BURN_TX_HASH = '0x...'
// if there were multiple token transfers in the withdrawal, you can enter the index here
const INDEX_OF_TOKEN_TRANSFER = '?'

const client = new Web3SideChainClient()
client.init(config).then(() => {
    const rootChain = new RootChain(client, '0x2890bA17EfE978480615e330ecB65333b880928e')
    const exit = new ExitUtil(client, rootChain)
    return exit.buildPayloadForExit(BURN_TX_HASH, TRANSFER_EVENT_SIG, false, INDEX_OF_TOKEN_TRANSFER).then(console.log)
})